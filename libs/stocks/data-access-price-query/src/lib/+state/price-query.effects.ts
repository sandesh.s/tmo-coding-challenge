import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  StocksAppConfig,
  StocksAppConfigToken
} from '@coding-challenge/stocks/data-access-app-config';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import {
  FetchPriceQuery,
  PriceQueryActionTypes,
  PriceQueryFetched,
  PriceQueryFetchError
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQuery$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQuery,
    {
      run: (action: FetchPriceQuery, state: PriceQueryPartialState) => {
        return this.httpClient
          .get<PriceQueryResponse[]>(
            `${this.env.apiURL}/beta/stock/${action.symbol}/chart/${
              this.fetchSelectedTime(action.period[0],action.period[1])
            }?token=${this.env.apiKey}`
          )
          .pipe(
            map(resp => resp.filter(({ date }) => {
              const selDate   = new Date(date);
              return selDate >= action.period[0] && selDate <= action.period[1];
            })),
            map(resp => new PriceQueryFetched(resp as PriceQueryResponse[]))
          );
      },

      onError: (action: FetchPriceQuery, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  private fetchSelectedTime(frmDate: Date, toDate: Date): string {
    const diffOfDays = (+toDate - +frmDate) / 86400000;
    let diffText: string;

    switch(true) {
      case (diffOfDays <= 30):
        diffText = '1m';
        break;
      case (diffOfDays <= 90):
        diffText = '3m';
        break;
      case (diffOfDays <= 180):
        diffText = '6m';
        break;
      case (diffOfDays <= 365):
        diffText = '1y';
        break;
      case (diffOfDays <= 730):
        diffText = '2y';
        break;
      default:
        diffText = 'max';
      break;
    }

    return diffText;
  }


  constructor(
    @Inject(StocksAppConfigToken) private env: StocksAppConfig,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {}
}
