import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  stockPickerForm: FormGroup;
  symbol: string;
  period: string;
  currDate: Date= new Date();



  quotes$ = this.priceQuery.priceQueries$;

  get formPeriod() {
    return this.stockPickerForm.get('period') as FormArray;
  }


  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      period: fb.array([
        [null, Validators.required],[null, Validators.required]
      ]
        )
    });
  }

  ngOnInit() {
    this.formPeriod.valueChanges.subscribe(([startDate, endDate]) => {
      if (startDate <= endDate || !startDate || !endDate) { return; }

      this.formPeriod.setValue([endDate, endDate], { emitEvent: false });
    });

  }

  ngOnInit() {
    this.stockPickerForm.valueChanges.subscribe(this.fetchQuote);
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, period} = this.stockPickerForm.value;
      this.priceQuery.fetchQuote(symbol, period);      
    }
  }


}
